import secrets
from flask import Flask
from flask_caching import Cache

SECRET_KEY = secrets.token_hex(10)

# API_BASE_URL = 'http://127.0.0.1:5000/api/'
# API_BASE_URL = 'http://195.148.21.89:5000/api/'  # This is the old one
API_BASE_URL = 'http://86.50.230.190:5000/api/'  # This is the new one


def create_app():
    app = Flask(__name__)

    app.config["CACHE_TYPE"] = "FileSystemCache"
    app.config["CACHE_DIR"] = "cache"
    app.config['SECRET_KEY'] = SECRET_KEY
    return app


app = create_app()
cache = Cache(app)
